Assets = new Mongo.Collection('assets');

function loadExampleAssets() {
  Assets.insert({
    name: 'Dell Latitude C630', location: 'Desk 23-C', value: '$1,275'
  });
  Assets.insert({
    name: 'Alienware SX-27', location: 'Desk 29-A', value: '$1,900'
  });
  Assets.insert({
    name: 'Cray-2', location: 'Sub-basement B', value: '$8,800,000'
  });
  Assets.insert({
    name: 'Commodore 64', location: 'Rec room', value: '$∞'
  });
  Assets.insert({
    name: 'Atari 2600', location: 'Media room', value: '$2,200'
  });
  Assets.insert({
    name: 'Tandy 1000', location: 'Office', value: '$75'
  });
}

Meteor.startup(function() {
  if (Assets.find().count() < 1) {
    loadExampleAssets();
  }
});
