Example [Meteor](https://www.meteor.com) app for managing hardware assets.

Written to demo/practice:

1. simple [CRUD](http://en.wikipedia.org/wiki/Create,_read,_update_and_delete)
   operations with Meteor
1. [Bootstrap 3](http://getbootstrap.com)

See also:

1. <https://simple-asset-manager.meteor.com> (live demo)
1. <http://stackoverflow.com/questions/27198927/how-does-event-currenttarget-input-value-give-me-an-input-value-in-a-meteor-fo/27201971>
