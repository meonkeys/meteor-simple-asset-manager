Assets = new Mongo.Collection('assets');

Template.assets.helpers({
  'assets': function() {
    return Assets.find();
  }
});

Template.assets.events({
  'click #deleteAsset': function(event, template) {
    Assets.remove(this._id);
  },
  'submit form': function(event, template) {
    event.preventDefault();
    Assets.insert({
      name: event.target.inputName.value,
      location: event.target.inputLocation.value,
      value: event.target.inputValue.value
    });
  }
});
